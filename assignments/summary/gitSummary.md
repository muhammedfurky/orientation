**Git** : *Git is a type of version control system used fro coordinating work among programmers. It is used for tracking changes in the source code during software development.*
**Repository** :*Repository is basically a big box of collection of files and folders (code files) in which you and your fellow teammates can store the codes.*
**Commit** : *Commit is basically saving your work. The commit only exists in your local machine until it is pushed to a remote repository.*
**Branch** : *Branches are separate instances of codes to be integrated to the main codebase.*
**Merge** : *Merging is basically integrating different branches to the main codebase*
**Cloning** : *Cloning is for making an exact copy of your online repository to your local machine.*
**Fork** : *By fork, you get an entirely new repository of the code under your name.*